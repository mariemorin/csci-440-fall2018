# Tutorial: Measuring Query Performance

In this tutorial, we will use a Bash script that runs each query 100,000 times and measures the execution time with three metrics (defined below). We will use the results from these experiments to demonstrate the practical effects of certain basic query optimization techniques. This tutorial should provide an introduction to query optimization and measurement, but may also serve as a platform that supports further independent exploration and experimentation.

## Prerequisites

This tutorial assumes the reader has a basic understanding of SQL commands as well as some experience working with sqlite3. The reader should be proficient in writing basic SQL queries and have experience with Bash scripting. Example programs in this tutorial are run in the terminal so the reader should be comfortable executing terminal commands. Below is a list of several important files, and their general purpose in this tutorial.

* `data.sql` - This file was generated using `DUMP` and builds a table called **Random** that has 26 columns and 17,575 rows
* `timing-script.sh` - This file takes a database and a query, and runs that query 100,000 times. Because the data is not refreshed after each execution, this script is intended for use with queries such as `SELECT` that do not modify the structure or contents of the database.
* `data_generator.py` - This file was used to generate the **Random** table and can be modified to produce different amounts of random data
* `sql_demo.py` - This file is an example of how to measure execution time using Python 3.7, if you are not comfortable with the Bash script mentioned above. Unlike `timing-script.sh` this only measures real time execution (think clock time) and only executes the query one time.

## Before You Begin

A simple Bash script is used in this tutorial to time the execution of SQL queries. For the examples, the following script, `timing-script.sh`, is used to execute each query 100,000 times:

    #!/bin/bash
    file=$1
    db=$2

    echo "Running $file 100,000 times"
    time (
        for i in 100000; do
            sqlite3 $db < $file > /dev/null
        done
    )

The timing script can be executed by:

    ./timing-script.sh <sql-file> <database-file>

To set up the database file for this tutorial, first load `data.sql` into a temporary database file using sqlite3:

    sqlite3 tmp.db < data.sql

If you are using Windows or would like to avoid a Bash script, you can accomplish all of the same experiments by modifying the included `sql_demo.py` script. However, we recommend using the Bash script with either the Windows Subsystem for Linux or Cygwin.

## Optimizing Your Queries
In this part of the tutorial, we will walk through a few common queries, how to optimize them, the differences in speed of the queries, and why you would want to optimize.

## The SELECT Query

The `SELECT` query is one of the most common queries, as you're selecting data to be returned by the rest of the query. It's easy to get far too much data back, which can end up costing a lot of wasted time, money, and resources.

### Defining Specific Fields, Instead of * (aka, all)

One easy query optimization is querying for less data. The results of selecting "all" data grows quickly. Imagine a small database with only 50 entries total, where each entry has 4 columns. This is at most 200 total data entries that will be returned— which is not much data at all! On **small** datasets, selecting all data may not be detrimental to the execution time of queries. However, it does not scale well. Imagine the same database, but with 200 entries— this will be returning 800 fields this time, and if another field is added, you'll be querying and returning 1,000 fields. It quickly grows out of hand and therefore, you should be querying only for necessary fields.

For example, imagine you're working on a database for a school of 400 students. The school wants to get the `Name`, `Address`, and `Grade` for each student. However, they are storing the `Name`, `Address`, `Grade`, `Email`, `Phone Number`, `Mother`, `Father`, and `GPA` of each student. When using the all (\*) symbol when selecting students, the query will return a total of 8,000 data entries when only 1,200 are needed. This is a significant difference, even for a relatively small database. Imagine working for a university of 20,000 students— this would introduce a huge amount of latency and waste a large amount of resources *(read: $$$)* on data that is not used.

    SELECT Name, Address, GPA
    FROM Student

Rather Than:

    SELECT *
    FROM Student

An example of the time/latency difference for the `SELECT` query can be shown below:
Here, I am searching for the fields **a-d** (4 fields) in our **Random** table, which is just filled with random values.

Using the query:

    SELECT a, b, c, d
    FROM Random

as opposed to the query:

    SELECT *
    FROM Random

can significantly decrease execution time when working with large datasets. The following example is used to demonstrate the effects of utilizing inefficient select statements:

### Example 1: SELECT

1. Create a new `.sql` file named `example1-no1.sql` with the query:

        SELECT * FROM Random;

2. Create a new `.sql` file named `example2-no2.sql` with the query:

        SELECT a, b, c, d FROM Random;

    In this query, we chose to select only the columns a, b, c, and d as an example. Other variations of the fields will yield similar results.

3. Using the timing Bash script, execute the first query 100,000 times with the generated database, `tmp.db`:

        ./timing-script.sh example1-no1.sql tmp.db

    The output should be similar to the following:

        real   0m0.091s
        user   0m0.082s
        sys    0m0.005s

4. Again, execute the timing script, this time using the second query:

        ./timing-script.sh example1-no2.sql tmp.db

    The output should be similar to the following:

        real 0m0.034s
        user 0m0.020s
        sys  0m0.007s

As noted in the output, query 2 is almost **3 times** as fast as query 1.

## The LIMIT Query

`LIMIT` can be useful when you only want a certain amount of data. This works similarly to the `SELECT` example above— you will be getting back a smaller amount of data than if you had no upper bound on the amount of data your program would be querying. A use case for `LIMIT` would be to ensure that the data you have is usable for whatever you're using it for— to continue with the school example above, you could use it to ensure that the GPA and Name fields aren't empty for students, and that it's returning what you're looking for.

### Example 2: LIMIT

1. Create a new `.sql` file named `example2-no1.sql` with the query:

        SELECT * FROM Random;

2. Create a new `.sql` file named `example2-no2.sql` with the query:

        SELECT * FROM Random;
        LIMIT 100;

    In this query, we are limiting the number of results to 100— far less than selecting ALL will do.

3. Using the timing Bash script, execute the first query 100,000 times with the generated database, `tmp.db`:

        ./timing-script.sh example2-no1.sql temp.db

    The output should be similar to the following:

        real 0m0.107s
        user 0m0.096s
        sys  0m0.007s

4. Again, execute the timing script, this time using the second query:

        ./timing-script.sh example1-no2.sql temp.db

    The output should be similar to the following:

        real 0m0.028s
        user 0m0.019s
        sys  0m0.006s

    It makes sense that limiting results will make our script faster— nearly 5 times faster, in this case for real time and user time. If it fits your use case and you don't require all the data, using the `LIMIT` function can make your queries MUCH faster.

## The WHERE Query

`WHERE` is a widely used and very powerful search condition in SQL. `WHERE` helps us find specifics in our data. In this example we're going to pair `SELECT`, `WHERE`, and `rowid` to pinpoint the specific rows that we want our query to output. Without `WHERE`, we would simply be mindlessly sifting through our dataset rather than specifically targeting a known objective. Doing this will optimize our query greatly and provide an easier way of accessing our data with this search condition.

### Example 3: WHERE

1. Create a new `.sql` file named `example3-no1.sql` with the query:

        SELECT * FROM Random;

2. Create a new `.sql` file named `example3-no2.sql` with the query:

        SELECT * FROM Random;
        WHERE rowid >= 300 AND rowid <= 600;

    Using this query, we are specifically looking for the rows 300-600 in our dataset by using the `AND` operator rather than searching our entire dataset.

3. Using the timing Bash script, execute the first query 100,000 times with the generated database, `tmp.db`:

        ./timing-script.sh example3-no1.sql tmp.db

    The output should be similar to the following:

        real   0m0.110s
        user   0m0.081s
        sys    0m0.018s

4. Again, execute the timing script, this time using the second query:

        ./timing-script.sh example3-no2.sql tmp.db

    The output should be similar to the following:

        real 0m0.015s
        user 0m0.003s
        sys  0m0.003s

By using `WHERE` to specify which rows we are specifically looking for, we reduced our search time for a specified constraint to 1/7th the time it takes to search our whole database. This makes `WHERE` one of the most powerful commands in SQL.

## The INNER JOIN Query

Joins in SQL can be done in several ways. One common way is to use an implicit `JOIN` and filter your results with a `WHERE` statement as in the below query. The implicit `JOIN` is achieved by using commas to `SELECT` from multiple tables at once.

### Example 4: INNER JOIN

Most modern DBMS will convert the following query (implicit `JOIN`) to an `INNER JOIN`. Depending on the complexity of the query, the DBMS may instead perform a `CROSS JOIN`. This produces an enormous number of records and *then* filters the rows for the data that was selected. For our relatively small **Random** database, the DBMS would create 308,915,776 rows when performing a `CROSS JOIN`.

    SELECT r1.a, r2.a
    FROM Random as r1, Random as r2
    WHERE r1.a > r2.a

To avoid this, it is recommended to write queries that use the explicit `INNER JOIN` syntax. This will never result in a `CROSS JOIN` and is safer to run on large databases. Create the file `example4-no1.sql` with the following query:

    SELECT r1.a, r2.a
    FROM Random as r1
    INNER JOIN Random as r2
    ON r1.a > r2.a;

When we run this with the script, we see that even an explicit `INNER JOIN` takes a while to execute.

    $ ./timing-script.sh example3-no2.sql temp.db
    Running example4-no2.sql 100,000 times

    real    1m59.436s
    user    1m59.196s
    sys     0m0.147s

 Running the query with the implicit `JOIN` from above has a similar running time, because our DBMS is smart enough to use an `INNER JOIN` when the `WHERE` statement is so simple.

Therefore, in an attempt to demonstrate the difference, we can simulate the worst case scenario by using an explicit `CROSS JOIN` in the file `example4-no2.sql`.

    SELECT r1.a, r2.a
    FROM Random as r1
    CROSS JOIN Random as r2
    ON r1.a > r2.a;

Running this with the timing script, we see that this takes roughly the same amount of time. This is a good thing, and is a result of the query optimizer working to make things as efficient as possible. The effects of using `CROSS JOIN` either explicitly or implicitly grow exponentially as the database grows, and the query optimizer may not work as well when working in a legacy system.

    $ ./timing-script.sh example3-no1.sql temp.db
    Running example4-no1.sql 100,000 times


    real    2m9.885s
    user    2m5.957s
    sys     0m0.609s


By using the `EXPLAIN` command, we can see how the query plan is executed at a fairly low level. This is demonstrated in `example4-no3.sql` and `example4-no4.sql` the outputs of each are below.

Example 3:

    sqlite> .read example4-no3.sql
    addr  opcode         p1    p2    p3    p4             p5  comment
    ----  -------------  ----  ----  ----  -------------  --  -------------
    0     Init           0     14    0                    00  Start at 14
    1     OpenRead       0     2     0     1              00  root=2 iDb=0; Random
    2     OpenRead       1     2     0     1              00  root=2 iDb=0; Random
    3     Rewind         0     13    0                    00
    4     Rewind         1     13    0                    00
    5     Column         0     0     1                    00  r[1]=Random.a
    6     Column         1     0     2                    00  r[2]=Random.a
    7     Le             2     11    1     (BINARY)       51  if r[1]<=r[2] goto 11
    8     Column         0     0     3                    00  r[3]=Random.a
    9     Column         1     0     4                    00  r[4]=Random.a
    10    ResultRow      3     2     0                    00  output=r[3..4]
    11    Next           1     5     0                    01
    12    Next           0     4     0                    01
    13    Halt           0     0     0                    00
    14    Transaction    0     0     1     0              01  usesStmtJournal=0
    15    Goto           0     1     0                    00

Example 4:

    sqlite> .read example4-no4.sql
    addr  opcode         p1    p2    p3    p4             p5  comment
    ----  -------------  ----  ----  ----  -------------  --  -------------
    0     Init           0     14    0                    00  Start at 14
    1     OpenRead       0     2     0     1              00  root=2 iDb=0; Random
    2     OpenRead       1     2     0     1              00  root=2 iDb=0; Random
    3     Rewind         0     13    0                    00
    4     Rewind         1     13    0                    00
    5     Column         0     0     1                    00  r[1]=Random.a
    6     Column         1     0     2                    00  r[2]=Random.a
    7     Le             2     11    1     (BINARY)       51  if r[1]<=r[2] goto 11
    8     Column         0     0     3                    00  r[3]=Random.a
    9     Column         1     0     4                    00  r[4]=Random.a
    10    ResultRow      3     2     0                    00  output=r[3..4]
    11    Next           1     5     0                    01
    12    Next           0     4     0                    01
    13    Halt           0     0     0                    00
    14    Transaction    0     0     1     0              01  usesStmtJournal=0
    15    Goto           0     1     0                    00

## Exercises for the Reader

Take the following queries on the **Random** table, and use the above techniques to improve them as much as you can. Each query may benefit from one or many of the discussed methods for query optimization. Good luck!

1. Query 1 - If we are only concerned about the values in `r1.a`, how can we filter our output such that we only get the data we need?

        SELECT *
        FROM Random as r1, Random as r2, Random as r3
        WHERE r1.a > r2.b > r3.c

2. Query 2 - If we are only concerned with the first 100 selected results, how can we filter our output such that we only get the data that we need?

        SELECT r1.a, r2.a, r3.a
        FROM Random as r1
        CROSS JOIN Random as r2
        CROSS JOIN Random as r3
        ON r1.a > r2.b > r3.c

3. Query 3 - If we are only concerned with entries in rows 1,000 - 1,500, how can we filter our output such that we only get the data that we need?

        SELECT * FROM Random as r1
        INNER JOIN Random as r2
        ON r1.a * r2.a > r1.c *  r2.d

## Further Reading

* [Query Optimization](https://en.wikipedia.org/wiki/Query_optimization)
* [Query Planning](https://www.sqlite.org/queryplanner.html)
* [SQLite: Where](http://www.sqlitetutorial.net/sqlite-where/)

## Consulted Sources

* [Sisense: 8 Ways to Fine Tune SQL Queries](https://www.sisense.com/blog/8-ways-fine-tune-sql-queries-production-databases/)
* [w3schools SQL reference/tutorial](https://www.w3schools.com/sql/default.asp)
* [SQLite by Chris Newman](https://www.safaribooksonline.com/library/view/sqlite/067232685X/)
