#!/bin/bash
file=$1
db=$2

echo "Running $file 100,000 times"
time (
    for i in 100000; do
        sqlite3 $db < $file > /dev/null
    done
)
