EXPLAIN SELECT r1.a, r2.a
FROM Random as r1
CROSS JOIN Random as r2
ON r1.a > r2.a;