#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# step 1: import sqlite3 and other libraries
import sqlite3
import time


def main():
    timestamps = {'start': time.time_ns()}

    # open a sqlite3 session and create a cursor
    conn = sqlite3.connect('temp.db')
    c = conn.cursor()

    c.execute('''
    select count(r1.a) from Random as r1
    where r1.a * r1.b > r1.c * r1.d
    ''')
    timestamps['query1'] = time.time_ns() - timestamps['start']
    print(c.fetchall())

    print(timestamps)


if __name__ == '__main__':
    main()
