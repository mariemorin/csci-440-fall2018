import pathlib, csv, random, time
from column_names import headers

start_time = time.time_ns()
filename = 'data.csv'

output = pathlib.Path(filename)
with open(output, 'w') as f:
    table = [headers]
    for i in range(len(headers)** 3):
        table.append([])
        for j in range(len(headers)):
            table[-1].append(random.randint(0, len(headers) ** 2))
        #     print(table[i][j], end='\t')
        # print()

    duration_ns = time.time_ns() - start_time
    print('finished generating in {}ns ({}s)'.format(duration_ns, duration_ns / 1000000000))

    writer = csv.writer(f, lineterminator='\r')
    writer.writerows(table)
# need to remove the last blank line from the output file