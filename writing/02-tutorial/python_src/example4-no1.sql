SELECT r1.a, r2.a
FROM Random as r1
INNER JOIN Random as r2
ON r1.a > r2.a;
