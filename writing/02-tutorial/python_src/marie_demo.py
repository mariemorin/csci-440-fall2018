#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# step 1: import sqlite3 and other libraries
import sqlite3
import time


def main():
    # open a sqlite3 session and create a cursor
    conn = sqlite3.connect('temp.db')
    c = conn.cursor()

    timestamps = {}

    timestamps['query1 start'] = time.time_ns()
    c.execute('''
    SELECT *
    FROM Random
    ''')
    timestamps['query1 runtime'] = time.time_ns() - timestamps['query1 start']

    timestamps['query2 start'] = time.time_ns()
    c.execute('''
    SELECT a, b, c, d
    FROM Random
    ''')
    timestamps['query2 runtime'] = time.time_ns() - timestamps['query2 start']

    print(timestamps)


if __name__ == '__main__':
    main()
