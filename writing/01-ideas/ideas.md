# Ideas

## Team Members

* Marie Morin
* Carie Pointer
* Trenton Baker
* Austin Johnson

## Measuring Query Performance
In this tutorial, we will run queries in some environment (python, java, etc.) that is able to measure the execution time of different queries. We will use these findings to demonstrate some basic query optimization techniques. This tutorial will focus on query optimization, but will touch on the use of SQL in other environments. These techniques can be used to programatically generate and modify data in a database. The value of this tutorial is it provides users with techniques they can use to cut down on execution time of queries. This is useful for large datasets with multiple queries executing concurrently, or queries that are time-sensitive. Additionally, we will compare perfrmance of an sql database and a basic multi-dimensional array in the chosen environment. This should demonstrate some of the benefits that can bring to a project that deals with large amounts of data.

## Refactoring an Outdated Database
In this tutorial, we will teach users how to take an old, legacy database and using the same data, can apply the best practices they will learn in the tutorial to refactor and improve the database design and storage. They will learn things like reusability, merging tables, and preventing 'double information' where a database stores the same information multiple times. This tutorial is valuable for users that may be required to update legacy databases. Understanding how to update and refactor existing databases is an important skill for any user that is tasked with merging new data into existing datasets.  

## SQL Injection/ Database Security
In this tutorial, we will talk about and explain some of the techniques used in SQL injection to maliciously access and (potentially) destroy a production database. We will teach readers how to fortify their databases, and protect against SQL Injection attacks in order to protect their data and their users' data as well. This tutorial will provide users with a basic understanding of database security, which is an important topic for designing both simple and advanced database systems. 
