.read data.sql
.headers on
.mode column

-- Highschooler(ID int, name text, grade int);
-- Friend(ID1 int, ID2 int);

-- name and grade of students
SELECT student.name, student.grade
FROM Highschooler student, Friend friend
-- whichever one is bigger
where student.ID = friend.ID1
GROUP BY friend.ID1 HAVING count(friend.ID2) = (
-- max friend count of current student
SELECT max(current_student.count) FROM (
SELECT count(ID2) AS count
FROM Friend GROUP BY ID1) AS current_student)
