.read data.sql
.headers on
.mode column

-- Friend(ID1 int, ID2 int);
-- selects no of students friends with cassandra, but NOT cassandra

--SELECT DISTINCT ID1
-- FROM (SELECT DISTINCT * FROM Friend WHERE ID1 = (
-- SELECT ID FROM Highschooler
-- WHERE name = 'Cassandra')
-- UNION
SELECT count(cass_friend.ID1) + count(friend_of_friend.ID2)
FROM Highschooler cass, Friend cass_friend, Friend friend_of_friend
WHERE cass.name = 'Cassandra'
AND cass.ID = cass_friend.ID1
AND friend_of_friend.ID1 = cass_friend.ID2
AND friend_of_friend.ID2 <> cass.ID
AND friend_of_friend.ID1 <> cass.ID
