.read data.sql
.headers on
.mode column

-- Take the average of friends count from ID2 in Friend table
select avg(friends) from (
    select count(ID2) as friends from Friend
    group by ID1);
