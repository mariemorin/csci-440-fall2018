.read data.sql
.headers on
.mode column

select name, grade
from Highschooler,
(select ID1 from Friend
Except
Select distinct Friend.ID1
From Friend, Highschooler H1, Highschooler H2
Where Friend.ID1 = H1.ID and Friend.ID2 = H2.ID
And H1.grade = H2.grade)
As temp
Where Highschooler.ID = temp.ID1;
