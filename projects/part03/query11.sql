.read data.sql
.headers on
.mode column

--The query returns a single column, with the pair of students in alphabetical
-- order. For example, the results:
-- Student 1
-- Student 2
-- Student 3
-- Student 4
-- means Student 1 likes Student 2 and Student 2 likes Student 1.
-- Student 3 likes Student 4 and Student 4 likes Student 3.

select name, grade from Highschooler
where ID in (
    select ID1 from Likes
        where ID1 in (select ID2 from Likes)
        and ID2 in (select ID1 from Likes)
) order by name;

