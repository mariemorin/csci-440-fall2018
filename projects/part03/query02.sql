.read data.sql
.headers on
.mode column

delete from Likes
where ID1 in (select Likes.ID1
from Friend join Likes using (ID1)
where Friend.ID2 = Likes.ID2)
and not ID2 in (select Likes.ID1
from Friend join Likes using (ID1)
where Friend.ID2 = Likes.ID2);
