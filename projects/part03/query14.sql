.read data.sql
.headers on
.mode column

select name, grade from Highschooler where ID in(
    select ID1 from Highschooler student, Friend, Highschooler friend
    where student.ID = Friend.ID1 -- only look at *their* friends, not who is friends with them
    and friend.ID = Friend.ID2 
    and student.grade = friend.grade)
order by grade, name;