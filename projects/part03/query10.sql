.read data.sql
.headers on
.mode column

select A.name, A.grade, B.name, B.grade from Highschooler as A
    inner join Likes
        on A.ID = Likes.ID1
    inner join Highschooler as B
        on B.ID = Likes.ID2
    where
        A.grade - B.grade >= 2
    or
        B.grade - A.grade >= 2;
