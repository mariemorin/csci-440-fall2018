.read data.sql
.headers on
.mode column

select H1.name, H1.grade, H2.name, H2.grade
	From Highschooler H1, Highschooler H2,
	(select ID1, ID2
	From Likes
	Where ID2 not in (select ID1 from Likes))
	As temp
	Where H1.ID = temp.ID1
	And H2.ID = temp.ID2;
