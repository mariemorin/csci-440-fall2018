.read data.sql

select A.name, A.grade, B.name, B.grade, C.name, C.grade
from Highschooler as A
inner join Likes as l
inner join Highschooler as B
inner join Friend as f1
inner join Friend as f2
inner join Highschooler as C
where A.ID = l.ID1
and l.ID2 = B.ID
and B.ID not in (
    select Friend.ID2 from Friend
    where Friend.ID1 = A.ID
    -- or Friend.ID2 = A.ID
    -- add/remove the above or statement for two/one sided friendship
    -- the results are the same for the provided data, but it could matter
)
and A.ID = f1.ID1
and B.ID = f2.ID2
and C.ID = f1.ID2
and C.ID = f2.ID1