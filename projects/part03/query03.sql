.read data.sql
.headers on
.mode column


insert into Friend
select f1.ID1, f2.ID2
from Friend f1 join Friend f2 on f1.ID2 = f2.ID1
where f1.ID1 <> f2.ID2
except
select * from Friend;
